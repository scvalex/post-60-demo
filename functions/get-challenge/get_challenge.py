import hashlib
import math
import os
import uuid
from datetime import datetime

def lambda_handler(event, context):
    secret_key = os.environ["SECRET_KEY"]

    prompt = str(math.floor(datetime.now().timestamp())) + "|" + str(uuid.uuid4())
    h = hashlib.blake2b(digest_size=16, key=secret_key.encode())
    h.update(prompt.encode())
    signature = h.hexdigest().encode("utf-8")

    return {
        "body": {
            "prompt": prompt,
            "signature": signature,
        },
        "statusCode": 200,
    }
