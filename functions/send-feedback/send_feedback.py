import base64
import hashlib
import hmac
import json
import math
import os
import smtplib
from datetime import datetime
from email.mime.text import MIMEText

def lambda_handler(event, context):
    # Get the secrets from the environment
    smtp_key = os.environ["SMTP_KEY"]
    secret_key = os.environ["SECRET_KEY"]

    # Parse the JSON payload
    try:
        data = json.loads(event["body"])
    except:
        return err("Invalid parameters")

    # Extract the parameters from the JSON payload
    comment = data.get("comment") or "No comment"
    prompt = data.get("prompt")
    signature = data.get("signature")
    answer = data.get("answer")
    feedback = data.get("feedback")
    path = data.get("path");
    if prompt is None or signature is None or answer is None or feedback is None or path is None:
        return err("Missing parameters")

    # Check that the prompt is valid
    h = hashlib.blake2b(digest_size=16, key=secret_key.encode())
    h.update(prompt.encode())
    expected_signature = h.hexdigest().encode("utf-8")
    if not hmac.compare_digest(expected_signature, signature.encode()):
        return err("Invalid prompt")

    # Ensure that the timestamp is recent enough
    now = math.floor(datetime.now().timestamp())
    answer_now = prompt.split("|")[0]
    if now > int(answer_now) + 10:
        return err("Too late")

    # Compute the challenge, and check that the digest of the
    # challenge and answer pass our test.
    challenge_and_answer = prompt + "Path" + path + "Feedback" + feedback + "Comment" + comment + "Answer" + answer
    digest = hashlib.sha256(challenge_and_answer.encode()).digest()
    if int.from_bytes(digest[:4], "big") >= 16777216:
        return err("Challenge failed; my challenge was " + challenge_and_answer)

    # All's good, so send out the feedback email.
    msg = MIMEText(
        "Feedback: " + feedback
        + "\nComment: " + comment
        + "\nURL: https://scvalex.net" + path)
    msg['Subject'] = "Feedback: " + path
    msg['From']    = "FROM-ADDRESS-FILL-ME-IN"
    msg['To']      = "TO-ADDRESS-FILL-ME-IN"
    s = smtplib.SMTP("SMTP-SERVER-FILL-ME-IN", 587)
    s.login("SMTP-USERNAME-FILL-ME-IN", smtp_key)
    s.sendmail(msg["From"], msg["To"], msg.as_string())
    s.quit()

    return {
        "body": "Ok",
        "statusCode": 200,
    }

def err(msg) :
    return {
        "body": msg,
        "statusCode": 403
    }
