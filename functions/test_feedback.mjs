import * as lib from '../feedback.js';
import { webcrypto } from 'crypto';
import 'https';
import { default as axios } from '../node_modules/axios/index.js';

const { subtle } = webcrypto;

let resp = await axios.get('https://GET-CHALLENGE-URL.lambda-url.eu-north-1.on.aws/');
let challenge = resp.data;
let feedback = "Good";
let comment = "You did good";
let path = "/posts/60/";
let answer = await lib.solveChallenge(subtle, challenge.prompt, path, feedback, comment);
resp = await axios({
    method: 'post',
    url: 'https://SEND-FEEDBACK-URL.lambda-url.eu-north-1.on.aws/',
    data: {
        "feedback": feedback,
        "prompt": challenge.prompt,
        "signature": challenge.signature,
        "answer": answer,
        "comment": comment,
        "path": path,
    }
}) ;
console.log(resp.data);
