async function digestMessage(crypto, message) {
    const encoder = new TextEncoder();
    const data = encoder.encode(message);
    const hash = await crypto.digest('SHA-256', data);
    return hash;
}

async function solveChallenge(crypto, prompt, path, feedback, comment) {
    let answer = '';
    for (;;) {
        answer = '' + Math.random();
        let challenge_and_answer =
            prompt + "Path" + path + "Feedback" + feedback
            + "Comment" + (comment || "No comment") + "Answer" + answer;
        let digest = new DataView(await digestMessage(
            crypto,
            challenge_and_answer
        ));
        if (digest.getUint32(0, false) < 16777216) {
            break;
        }
    }
    return answer;
}

if (typeof window !== "undefined") {
    window.addEventListener("DOMContentLoaded", (event) => {
        let form = document.querySelector("#feedbackForm");
        let formTrigger = document.querySelector("input[value='Send']");
        let commentElement = document.querySelector("textarea[name='comment']");
        commentElement.addEventListener("keydown", function(event) {
            if ((event.ctrlKey || event.metaKey)
                && (event.keyCode == 13 || event.keyCode == 10)) {
                form.dispatchEvent(new SubmitEvent("submit", {
                    submitter: formTrigger,
                    cancelable: true
                }));
            }
        });
        form.addEventListener("submit", function(event) {
            event.preventDefault();
            let feedbackElement = document.querySelector("input[name='rating']:checked");
            let feedback;
            if (feedbackElement) {
                feedback = feedbackElement.value;
                ratingWrapper.classList.remove("border-2");
                formTrigger.disabled = true;
                let comment = commentElement.value;
                let path = document.location.pathname;
                fetch("https://gsa2eajfom4frhojhloqk6ioi40ahekk.lambda-url.eu-north-1.on.aws/")
                    .then(response => response.json())
                    .catch(error => {
                        console.log("Error getting challenge", error);
                        formTrigger.disabled = false;
                    })
                    .then(challenge => solveChallenge(crypto.subtle, challenge.prompt, path, feedback, comment)
                          .then(answer => {
                              return {challenge: challenge, answer: answer};
                          }))
                    .then(({challenge, answer}) =>
                        fetch("https://in5c4y6kp4hjubcc6ifhafszla0dasib.lambda-url.eu-north-1.on.aws/", {
                            method: "post",
                            headers: {
                                "Content-Type": "application/json",
                            },
                            body: JSON.stringify({
                                "feedback": feedback,
                                "prompt": challenge.prompt,
                                "signature": challenge.signature,
                                "answer": answer,
                                "comment": comment,
                                "path": path,
                            })
                        }))
                    .then(response => response.text())
                    .catch(error => {
                        console.log("Error sending feedback", error);
                        formTrigger.disabled = false;
                    })
                    .then(data => {
                        console.log("Feedback response:", data);
                        formTrigger.value = "Sent";
                    });
            } else {
                let ratingWrapper = document.querySelector("#ratingWrapper");
                ratingWrapper.classList.add("border-2");
            }
        });
    })
}

if (module === undefined) {
    var module = {
        exports: {},
    };
}

module.exports = {
    solveChallenge: solveChallenge,
};
