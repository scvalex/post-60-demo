# post-60-demo

> Demo code for https://scvalex.net/posts/60/

This repo contains the AWS Lambda functions that implement the
proof-of-work feedback system, and the Javascript to add it to an HTML
form.

This doesn't include any deployment scripts, and the HTML is just the
simple Liquid template that I use.  If you want to use the code,
you'll have to figure these bits out for yourself.
